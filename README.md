# mccullough.xyz

Deploy configuration for my website at [https://mccullough.xyz](https://mccullough.xyz)

Deploys Wordpress with MariaDB - intended to be deployed to an arm32 cluster.

## Usage

Create a namespace, password secret, and apply the deployments.

```
kubectl create namespace blog
kubectl create secret generic mysql-auth --from-literal=password=$MYSQL_PASSWORD
kubectl apply -f ./deploy
```